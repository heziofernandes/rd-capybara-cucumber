@segmentationDelete
Feature: Delete a Segmentation of the system,
  In order to use the system
  As user
  I want to Delete a Segmentation in the system

  Background: Access the system
    Given the user is logged in

  Scenario: Performs registration form
    When search for a Segmentation
    And delete a specific segmentation
    Then segmentation will no longer be present in the system