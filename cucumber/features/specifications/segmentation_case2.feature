@segmentationEdition
Feature: Edit a Segmentation in the system,
  In order to use the system
  As user
  I want to edit a Segmentation in the system

  Background: Access the system
    Given the user is logged in

  Scenario: Performs registration form
    When edit a existing Segmentation
    And enter a new name for Segmentation
    And delete the second filter segmentation
    And select the first parameter with  Lead Scoring
    And the next as Interest
    And the next with not equals
    And the next with numeric some value
    And save the edited segmentation
    Then he edited segmentation is saved is listed in the system