@segmentationRegister
Feature: Register a new Segmentation in the system,
  In order to use the system
  As user
  I want to create a new Segmentation in the system

  Background: Access the system
    Given the user is logged in

  Scenario: Performs registration form
    When Start creating Segmentation
    And enter a name for Segmentation
    And select the first parameter with funnel
    And the next as sale
    And the next with the Lead which has no sale
    When adding a further condition
    And select the first parameter with conversion
    And the next as not converted Leads
    And the next parameter as Exactly
    And fill the next parameter with 30
    And save the segmentation
    Then the segmentation is saved is listed in the system