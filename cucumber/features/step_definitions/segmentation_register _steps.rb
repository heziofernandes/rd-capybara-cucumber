# encoding: utf-8
Given(/^the user is logged in$/) do
    visit "https://app.rdstation.com.br/nova-segmentacao"
    fill_in 'user_email', with: 'fernandes.bcc@gmail.com'
    fill_in 'user_password', with: 'teste000'
    find(:xpath, "//input[contains(@name, 'commit')]").click()
end

When(/^Start creating Segmentation$/) do
    find(:xpath, "//a[contains(@href, '/nova-segmentacao/nova')]").click()
end

When(/^enter a name for Segmentation$/) do
    fill_in 'segmentation_list_name', with: 'Test Segmentation'
end

When(/^select the first parameter with funnel$/) do
 find(:xpath,"//*[contains(text(),'Funil')]").click
end

When(/^the next as sale$/) do
    find(:xpath,"//*[contains(text(),'Venda')]").click
end

When(/^the next with the Lead which has no sale$/) do
    find(:xpath,"//*[contains(text(),'O Lead não possui venda')]").click
end

When(/^adding a further condition$/) do
     find(:xpath, "//a[contains(@class, 'js-add-condition')]").click()
end

When(/^select the first parameter with conversion$/) do
   all(:xpath,"//*[contains(text(),'Conversão')]")[1].click
end

When(/^the next as not converted Leads$/) do
    find(:xpath,"//*[contains(text(),'O Lead não converteu')]").click
end

When(/^the next parameter as Exactly$/) do
    find(:xpath,"//*[contains(text(),'exatamente')]").click
end

When(/^fill the next parameter with (\d+)$/) do |arg1|
    find(:xpath, "//input[@type='number']").set('30')
end

When(/^save the segmentation$/) do
    find(:id, "save-segmentation").click
end

Then(/^the segmentation is saved is listed in the system$/) do
   find(:xpath, "//input[@type='search']").set('Test Segmentation')
   page.has_css?('class.clickable', :text => 'Test Segmentation', :visible => true)
end

