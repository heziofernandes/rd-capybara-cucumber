
# encoding: utf-8
When(/^edit a existing Segmentation$/) do
  find(:xpath, "//input[@type='search']").set('Test Segmentation')
   page.has_css?('class.clickable', :text => 'Test Segmentation', :visible => true)
   find(:xpath,"//a[contains(text(),'Editar')]").click
end

When(/^enter a new name for Segmentation$/) do
  fill_in 'segmentation_list_name', with: 'Test Hezio Segmentation'
end

When(/^select the first parameter with  Lead Scoring$/) do
 find(:xpath,"//*[contains(text(),'Lead Scoring')]").click
end


When(/^the next as Interest$/) do
 find(:xpath,"//*[contains(text(),'Interesse')]").click
end

When(/^the next with not equals$/) do
 find(:xpath,"//*[contains(text(),'não é igual a')]").click
end

When(/^delete the second filter segmentation$/) do
    find(:xpath,"//*[@id=\"content\"]/div[1]/form/div[3]/div/div[1]/div[2]/div[2]/div[1]/a").click
end

When(/^the next with numeric some value$/) do
    find(:xpath, "//input[@type='number']").set('0')
end

When(/^save the edited segmentation$/) do
  find(:id, "save-segmentation").click
end

Then(/^he edited segmentation is saved is listed in the system$/) do
   find(:xpath, "//input[@type='search']").set('Test Hezio Segmentation')
   page.has_css?('class.clickable', :text => 'Test Hezio Segmentation', :visible => true)
end
