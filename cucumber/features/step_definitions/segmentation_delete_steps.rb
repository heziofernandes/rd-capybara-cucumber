# encoding: utf-8
When(/^search for a Segmentation$/) do
 find(:xpath, "//input[@type='search']").set('Test Hezio Segmentation')
    page.has_css?('class.clickable', :text => 'Test Hezio Segmentation', :visible => true)
end

When(/^delete a specific segmentation$/) do
       find(:xpath, "//*[@id=\"table-dynamiclist-index\"]/tbody/tr/td[2]/div/button").click
       find(:xpath, "//a[@class='exclude-list-link']").click
       page.driver.browser.switch_to.alert.accept

 end

Then(/^segmentation will no longer be present in the system$/) do
    find(:xpath, "//input[@type='search']").set('Test Hezio Segmentation')
    page.has_css?('class.clickable', :text => 'Test Hezio Segmentation', :visible => false)
end
