**rd-capybara-cucumber
**
Project using Cabybara ,Cucumber and Ruby.
It contains three cases of segmentation functionality tests.
- Segmentation Register 
- Segmentation Edition
- Segmentation Delete
To run the tests just use the cucumber command in the /rd-ruby-cucumber/cucumbe

=======================================================================
**Installing Ruby
**
sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

Installing with rbenv is a simple two step process. First you install rbenv, and then ruby-build:

cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

rbenv install 2.3.1
rbenv global 2.3.1
ruby -v
The last step is to install Bundler

gem install bundler
rbenv users need to run rbenv rehash after installing bundler.